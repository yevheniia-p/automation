package com.pekareva.lab3;

/**
 * Created by pekareva on 4/12/2016.
 */
public class ArrayComparison {
    public static void main(String[] args){
        int[] arr = {25, 51, 3, 71};
        int min = arr[0];
        for (int i = 0; i < arr.length; i++){
            if (arr[i] < min) {
                min = arr[i];
            }
        }
        System.out.println("The minimum is: " + min);
    }
}
