package com.pekareva.lab3;

/**
 * Created by pekareva on 4/12/2016.
 */
public class OutputArray {
    public static void main(String[] args){
        int[] arr = {1, 6, 3, 4};
        for (int i = 0; i < arr.length; i++)
            System.out.println(arr[i]);
    }
}
