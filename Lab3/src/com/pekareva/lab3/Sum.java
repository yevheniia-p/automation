package com.pekareva.lab3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by pekareva on 4/12/2016.
 */
public class Sum {
    public static void main(String[] args) {
        String value = null;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int sum = 0;

        while (true) {
            System.out.println("Enter number or \"Sum\" ");
            try {
                value = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (!"Sum".equals(value)) {
                sum += Integer.parseInt(value);
                System.out.println("Sum of the entered values is: " + sum);
            } else {
                break;
            }

        }
    }
}
