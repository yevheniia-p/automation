package com.pekareva.lab3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by pekareva on 4/12/2016.
 */
public class SimpleNumbers {
    public static void main(String[] args) {
        System.out.println("Enter value: ");
        BufferedReader n = new BufferedReader(new InputStreamReader(System.in));
        String n1 = null;
        try {
            n1 = n.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Integer input = Integer.parseInt(n1);
        for (int i = 2; i <= input; i++){
            if (isPrime(i)){
                System.out.println(i);
            }
        }

        }
    public static boolean isPrime(int number){
        for (int j = 2; j*j <= number; j++){
            if ((number % j) == 0) {
                return false;
            }
        }
        return true;
    }
}
