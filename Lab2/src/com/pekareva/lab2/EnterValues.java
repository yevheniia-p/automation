package com.pekareva.lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

/**
 * Created by User on 09.04.2016.
 */
public class EnterValues {
    public static void main(String[] args){
        System.out.println("Enter the first value: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String value1 = null;
        try {
            value1 = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        float v1 = Float.parseFloat(value1);

        System.out.println("Enter +, -, /, *");
        String sign = null;
        try {
            sign = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        char s1 = sign.charAt(0);

        System.out.println("Enter the second value: ");
        String value2 = null;
        try {
            value2 = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        float v2 = Float.parseFloat(value2);

        String t = "Result is: ";
        DecimalFormat df = new DecimalFormat();

        switch (s1){
            case '+' : System.out.println(t + df.format(v1 + v2));
                break;
            case '-' : System.out.println(t + df.format(v1 - v2));
                break;
            case '*': System.out.println(t + df.format(v1 * v2));
                break;
            case '/':
                if (s1 == 0){
                    System.out.println("Division by zero is not allowed!!!");
                }
                else {
                    System.out.println(t + df.format(v1 / v2));
                }
                break;
            default : System.out.println("Oooops, an error occurred while processing you request =( . Please check sign you entered!");
                break;
        }
    }
}
