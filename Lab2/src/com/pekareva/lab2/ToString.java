package com.pekareva.lab2;

/**
 * Created by pekareva on 4/8/2016.
 */
public class ToString {
    public static void main(String[] args) {
        //int to String
        int i = 10;
        String s1 = Integer.toString(i);
        System.out.println(10 + s1);
        //String to int
        Integer i1 = new Integer(s1);
        System.out.println(i1 + 10);

        System.out.println();

        //double to String
        double d = 10.3e10;
        String s2 = Double.toString(d);
        System.out.println(10 + s2);
        try {
            Double d1 = new Double(s2);
            System.out.println(d1 + 10);
        } catch (NumberFormatException e) {
            System.err.println("Incorrect type of the string");
        }

        System.out.println();

        //long to String
        long lng = 6593217;
        String s3 = Long.toString(lng);
        System.out.println(10 + s3);
        try {
            long lng2 = Long.valueOf(s3);
            System.out.println(lng2 + 1070980);
        } catch (NumberFormatException e){
            System.err.println("Incorrect type of the string");
        }

        System.out.println();

        //float to String
        float f = 69.17f;
        String s4 = Float.toString(f);
        System.out.println(10 + s4);
        try{
            float f2 = Float.parseFloat(s4);
            System.out.println(f2 + 10);
        } catch (NumberFormatException e){
            System.err.println("Incorrect type of the string");
        }

        System.out.println();

        //char to String
        char c1 = '*';
        String s5 = Character.toString(c1);
        System.out.println(10 + s5);
        char c11 = s5.charAt(0);
        System.out.println(c11);

        System.out.println();

        char[] c2 = new char[] {'1', '7', '$', '5'};
        String s6 = new String (c2);
        System.out.println(s6);
        char[] c22 = s6.toCharArray();
        System.out.println(c22);

        //

    }

}
