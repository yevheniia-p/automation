package com.pekareva.lab2;

import java.util.Random;

/**
 * Created by pekareva on 4/8/2016.
 */
public class Operators {
    public static void main(String[] args){
        int i = 3;
        float j = 7.7f;
        char k = '@';
        String s = "14";
        int y = 16;
        System.out.println("k + s = " + k + s);
        System.out.println("i / j = " + (i / j));
        System.out.println("remainder of the division y/i: " + (y % i));
        float f;
        f = i + j;
        int m = 15;
        if (f == m)
            System.out.println("f = m");
        else
            System.out.println("f != m");
        f += 2;
        System.out.println("f = " + f);

        for (int cr = 3; cr < 5; cr++)
            System.out.println(++cr);
        System.out.println(++f);
        f *= f;
        System.out.println(++f);
        System.out.println();

        if (f >= 7)
            System.out.println("f >= 7");

        boolean b = logic1(0) && logic2(7);
        System.out.println(b);
        boolean d = logic1(0) || logic2(7);
        System.out.println(d);
    }
    public static boolean logic1(int val) {
        System.out.println(val);
        System.out.println("Result: " + (val < 5));
        return val < 5;
    }
    public static boolean logic2(int val) {
        System.out.println(val);
        System.out.println("Result: " + (val < 3));
        return val < 3;
    }

}
