package com.pekareva.lab2;

/**
 * Created by User on 09.04.2016.
 */
public class IntToFloat {
    public static void main(String[] args){
        int i = 7;
        System.out.println(i);
        float f = i;
        System.out.println(f);
        int i2;
        i2 = (int) f;
        System.out.println(i2);

        System.out.println();

        float f2 = (float) 54.4;
        int i3 = Math.round(f2);
        System.out.println(i3);

        double d = 6.87676;
        int i4 = (int) d;
        System.out.println(i4);

        Double d2 = 6.555;
        int i5 = d2.intValue();
        System.out.println(i5);

        int i6 = Math.toIntExact(Math.round(d2));
        System.out.println(i6);

    }

}
